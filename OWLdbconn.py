# -*- coding: utf-8 -*-
#!/usr/bin/python3.4
import time
import datetime
import sqlite3
import os.path
import config
import sys


class TimeStamp(object):

    def __init__(self):
        unixtime = 0.0000000
        nowtime = ""

    def now_time(self):
        unixtime = time.time()
        nowtime = datetime.datetime.fromtimestamp(unixtime).strftime(
            '%Y-%m-%d %H:%M:%S')
        return nowtime

    def set_time(self, yyyymoddhhmi):
        print((yyyymoddhhmi))
        yyyymoddhhmi = int(str(yyyymoddhhmi[0:4] + yyyymoddhhmi[4:7] +
        yyyymoddhhmi[7:10] + yyyymoddhhmi[10:13] + yyyymoddhhmi[13:16]))
        print((yyyymoddhhmi))
        timeset = datetime.datetime(int(yyyymoddhhmi / 100000000),
             (int(yyyymoddhhmi / 1000000) % 100),
              (int(yyyymoddhhmi / 10000) % 100),
               (int(yyyymoddhhmi / 100) % 100),
                (yyyymoddhhmi % 100)).strftime('%Y.%m.%d:%H:%M')
        return timeset


class OWLtasksDb(object):
    db_path = ""
    taskdb = None

    def __init__(self, db):
        self.db_path = db
        if(os.path.exists(self.db_path)):
            taskdb = sqlite3.connect(self.db_path)
            cursor = taskdb.cursor()
        else:
            taskdb = sqlite3.connect(self.db_path)
            cursor = taskdb.cursor()
            cursor.execute("""
                CREATE TABLE project(
                    projectID   INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name        TEXT,
                    description TEXT,
                    deadline    DATE
)
""")
            cursor.execute("""
                CREATE TABLE task(
                    id           INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    priority     INTEGER DEFAULT 1,
                    details      TEXT,
                    status       TEXT,
                    deadline     DATE,
                    completed_on DATE,
                    project      TEXT NOT NULL REFERENCES project(name)
            )
""")
            print("I am commited!")
            taskdb.commit()
            print("I am commited!")

    def addproject(db, name, description, deadline):  # .
        db_path = config.db
        taskdb = sqlite3.connect(db_path)
        cursor = taskdb.cursor()
        #deadline = thetime.set_time(deadline)
        project = name, description, deadline

        print ('Inserting initial data')

        cursor.execute("""
        insert into project (
            name,
            description,
            deadline)
        values (
            ?,
            ?,
            ?)
        """, project)

    def dbaddtask(db, details, deadline):  # .
        db_path = config.db
        taskdb = sqlite3.connect(db_path)
        cursor = taskdb.cursor()
        #deadline = thetime.set_time(deadline)
        project = 'Owl Test'
        task = details, deadline, project

        print ('Inserting initial data')

        cursor.execute("""
        insert into task (
            details,
            deadline,
            project)
        values (
            ?,
            ?,
            ?)
        """, task)

        taskdb.commit()
        print("I AM VERY COMMITED!!!")

    def showprojects(db):
        db_path = config.db
        taskdb = sqlite3.connect(db_path)
        cursor = taskdb.cursor()
        cursor.execute("""
            select id,
            priority,
            details,
            status,
            deadline
            from task
        """)

        for row in cursor.fetchall():
            task_id, priority, details, status, deadline = row
            print (('%2d {%d} %-20s [%-8s] (%s)'
            % (task_id,
                priority,
                details,
                status,
                deadline)))

    def alltasks(db):
        db_path = config.db
        taskdb = sqlite3.connect(db_path)
        cursor = taskdb.cursor()
        cursor.execute("""
            select id,
            priority,
            details,
            status,
            deadline
            from task
        """)
        tasklist = []

        for row in cursor.fetchall():
            task_id, priority, details, status, deadline = row
            singletask = [task_id, priority, details, status, deadline]
            singletask[0] = int(singletask[0])
            singletask[1] = int(singletask[1])
            singletask[4] = str(singletask[4])
            singletask[2] = str(singletask[2])
            singletask[3] = str(singletask[3])
            print((singletask))
            #print (('%2d {%d} %-20s [%-8s] (%s)'
            #% (task_id,
                #priority,
                #details,
                #status,
                #deadline)))
            tasklist.append(singletask)
        print((tasklist))
        return tasklist


thetime = TimeStamp()
dbtest = OWLtasksDb(config.db)
#dbtest.addproject("Owl Test", "Christopher Lee Murray", 201507152143)
#dbtest.addtask("It seems to be working", 201507152143)
dbtest.alltasks()
#print(("The timestamp at execution is " + (thetime.now_time())))
#print(("The time set is " + (thetime.set_time(201507152143))))
