OWLY.do
=========
This is a simple todo app which uses sqlite3 for the database, and GTK+ as the UI from gi repositories. It is built with python3 and meant to be minimal. it is however in alpha, so there are a few issues.

Screenshot
---------
![alt text][screenie1]
[screenie1]: https://gitlab.com/thinkking/owly.do/raw/master/screenshots/Screenshot_2016-03-24_09-17-20.png

Functions
----------
+ portrait mode(+)
+ add tasks(+)
+ add due date(+)
+ sqlite3 read and write(+)
- delete(-)
- extended mode(-)
- reminders(-)
- fix layout (truncating)(-)
- build script (-)

to run:
```bash
git clone https://gitlab.com/thinkking/owly.do.git
cd owly.do
python3 OWL.py
```
