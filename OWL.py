#!/usr/bin/python3
from gi.repository import Gtk, GObject, Gio, Granite
import os.path, sys, io, codecs
import config, OWLdbconn
from OWL_Calvin import Calvin


class OWL(Gtk.Window):
    file_path = ""
    clear_btn = Gtk.Button()
    todoitems = []
    checkbox = None
    newcheckitem_entry = None
    newline = "\n"

    def __init__(self, fp):
        Gtk.Window.__init__(self, title="OWL Work Lab")
        self.set_border_width(5)
        self.stick()

        #windows size
        self.checkbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1)
        self.listbox = Gtk.ListBox()
        self.scroll = Gtk.ScrolledWindow()
        self.scroll.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.scroll.add(self.listbox)
        self.checkbox.pack_start(self.scroll, True, True, 0)
        self.add(self.checkbox)

        #header bar (hb) building
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "OWL Work Lab"
        self.set_titlebar(hb)
        self.fp = fp
        #main stuff
        self.ConnUpdate(fp)

        try:
            self.set_icon_from_file("OWL.png")
        except Exception as e:
            print ((e.message))
            sys.exit(1)

        #headerbar packing

            #right btutton set //autopilot & gears
        gears_btn = Gtk.Button()
        gears_icon = Gio.ThemedIcon(name="preferences-system-symbolic")
        gears_image = Gtk.Image.new_from_gicon(gears_icon, Gtk.IconSize.BUTTON)
        gears_btn.add(gears_image)
        gears_btn.set_tooltip_text("Gears")
        hb.pack_end(gears_btn)

        apilot_btn = Gtk.Button()
        apilot_icon = Gio.ThemedIcon(name="airplane-mode-symbolic")
        apilot_image = Gtk.Image.new_from_gicon(apilot_icon,
             Gtk.IconSize.BUTTON)
        apilot_btn.add(apilot_image)
        apilot_btn.set_tooltip_text("Auto Pilot")
        hb.pack_end(apilot_btn)

            #left button set //Calendar & Progress
        viewset_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(viewset_box.get_style_context(), "linked")

        cal_btn = Gtk.Button()
        cal_icon = Gio.ThemedIcon(name="appointment-new-symbolic")
        cal_image = Gtk.Image.new_from_gicon(cal_icon, Gtk.IconSize.BUTTON)
        cal_btn.add(cal_image)
        cal_btn.set_tooltip_text("Call Calvin")
        viewset_box.add(cal_btn)
        #cal_btn.connect("clicked", Calvin(), None)

        oversight_btn = Gtk.Button()
        oversight_icon = Gio.ThemedIcon(name="insert-object-symbolic")
        oversight_image = Gtk.Image.new_from_gicon(oversight_icon,
             Gtk.IconSize.BUTTON)
        oversight_btn.add(oversight_image)
        oversight_btn.set_tooltip_text("OverSight")
        viewset_box.add(oversight_btn)

        hb.pack_start(viewset_box)

                #creating ControlBar
        controlbar = Gtk.HButtonBox()
        self.datetimepicker = Gtk.HButtonBox()
        newcheckitem_btn = Gtk.Button()
        #newcheckitem_btn = Granite.WidgetsAppMenu()
        newcheckitem_icon = Gio.ThemedIcon(name="tab-new-symbolic")
        newcheckitem_image = Gtk.Image.new_from_gicon(newcheckitem_icon,
             Gtk.IconSize.BUTTON)
        newcheckitem_btn.add(newcheckitem_image)
        newcheckitem_btn.set_tooltip_text("New Item Details")
        controlbar.pack_start(newcheckitem_btn, True, False, 0)
        newcheckitem_btn.connect("clicked", self.addtask)

        tasks_progress = Gtk.ProgressBar()

        self.clear_btn = Gtk.Button()
        clear_icon = Gio.ThemedIcon(name="object-select-symbolic")
        self.clear_btn.connect("clicked", self.clear_checked)
        clear_image = Gtk.Image.new_from_gicon(clear_icon, Gtk.IconSize.BUTTON)
        self.clear_btn.add(clear_image)
        self.clear_btn.set_tooltip_text("Clear Checked Items")
        controlbar.pack_end(self.clear_btn, True, False, 0)

        self.newcheckitem_entry = Granite.WidgetsHintedEntry()
        self.newcheckitem_entry.connect('key-press-event', self.pushtodo)
        self.newcheckitem_entry.set_hint_string("Enter New Task")

        self.datepicker = Granite.WidgetsDatePicker.new_with_format("%y%m%d")
        #self.datepicker.set_hint_string("YYMMDD")
        self.timepicker = Granite.WidgetsTimePicker.new_with_format("%H%M",
             "%H%M")
        #self.timepicker.set_hint_string("HHMM")
        self.datetimepicker.pack_end(self.datepicker, True, False, 0)
        self.datetimepicker.pack_end(self.timepicker, True, False, 0)

        self.checkbox.pack_end(tasks_progress, False, True, 0)
        self.checkbox.pack_end(self.newcheckitem_entry, False, True, 0)
        self.checkbox.pack_end(self.datetimepicker, False, False, 0)
        self.checkbox.pack_end(controlbar, False, False, 0)

    def ConnUpdate(self, fp):
        self.file_path = fp
        count = 0
        print("0")
        if(os.path.exists(self.file_path)):
            tdfile = codecs.open(self.file_path, "r")
            print (tdfile)
            print("1")
            #tdfile_list = tdfile.readlines()
            tdfile_list = [line.rstrip(self.newline) for line in tdfile]

            print("2")
            #tdfile_list.append(line.rstrip('\n')for line in tdfile)
            print (tdfile_list)
            tdfile_list = OWLdbconn.dbtest.alltasks()
            print("2 db")
            print (tdfile_list)
            todo = []
            for todo in tdfile_list:
                print("2.0")
                print((todo[4]))
                #print((OWLdbconn.thetime.set_time((todo[4]))))
                #line = ("{:<50s}".format(str(todo[2]))
                #+ "\t" + "{:>10s}".format("Due: " + str(OWLdbconn.thetime.set_time((
                    #todo[4])))))  # remove when testing db adding
                task = ((todo[2]))
                print((todo[4]))
                deadline = ((todo[4]))
                self.addbutton(task, deadline, 0)
                count = count + 1
                print(("count is " + str(count)))
                self.todoitems.append(todo)
                print ((self.todoitems))
            print("3")
            tdfile.close()

    def addtask(self, widget):
        self.todoitems.append([self.newcheckitem_entry.get_text(),
             (str(20000000 + int(self.datepicker.get_text())) +
              str(self.timepicker.get_text()))])
        self.addbutton(self.newcheckitem_entry.get_text(),
             OWLdbconn.thetime.set_time((int(str(20000000 +
             int(self.datepicker.get_text())) +
              str(self.timepicker.get_text())))), 2)
        self.newcheckitem_entry.set_text("")

    def pushtodo(self, widget, entry):
        if entry.keyval == 65293:  # if enter is pressed
            OWLdbconn.dbtest.dbaddtask(self.newcheckitem_entry.get_text(),
                 OWLdbconn.thetime.set_time((str(20000000 +
             int(self.datepicker.get_text())) +
              str(self.timepicker.get_text()))))
            self.todoitems.append([self.newcheckitem_entry.get_text(),
                 (str(20000000 + int(self.datepicker.get_text())) +
              str(self.timepicker.get_text()))])
            #print(str(20000000 + int(self.datepicker.get_text())))
            self.addbutton(self.newcheckitem_entry.get_text(),
                 OWLdbconn.thetime.set_time((str(20000000 +
             int(self.datepicker.get_text())) +
              str(self.timepicker.get_text()))), 2)
              # get text from entry and add it to the label
            widget.set_text("")  # text back text to blank
            return True
        return False

    def addbutton(self, label, deadline, mode):
        if(label.strip() == ""):
            return
         #!!!!! make tdfile_list public andcheck if tdfile_list is empty
        if label.strip() not in self.todoitems:
            self.clear_btn.show()
            button = Gtk.CheckButton(label)
            boxTimeDate = Gtk.HButtonBox()
            listTimeDate = Gtk.ListBoxRow()
            boxTimeDate.pack_start(button, True, False, 0)
            deadline = Gtk.Label(deadline)
            deadline.set_width_chars(30)
            boxTimeDate.pack_start(deadline, True, False, 0)
            listTimeDate.add(boxTimeDate)
            if ((mode == 0) | (mode == 1)):
                print ("add")
                self.listbox.add(listTimeDate)
            elif (mode == 2):
                print ("prepend")
                self.listbox.prepend(listTimeDate)
                self.listbox.show_all()
            print ("(self.todoitems)")
            print ((self.todoitems))
            #if (mode == 1):
            #    self.ConnUpdate(self.fp)
            button.show()

    def clear_checked(self, widget):
        for v in self.checkbox:
            if(type(v) == Gtk.CheckButton):
                if(v.get_active() is True):
                    self.poptodo(v)

    def poptodo(self, widget):
        self.listbox.remove((widget.get_selected_rows ()))
        if not self.todoitems:
            self.clear_btn.hide()

    def destroy(self, event, param=None):
        #tdfile = io.open(self.file_path, 'w')
        ##tdfile.writelines([lining.encoding('utf-8') for lining in line] + self.newline for line in self.todoitems)
        #print("closing")
        #print((self.todoitems))
        #tdfile.writelines(u'\n'.join(line) for line in self.todoitems)
        #tdfile.close()
        Gtk.main_quit()


def main():
    win = OWL(config.fp)
    win.set_default_size(300, 700)
    win.set_default_geometry(300, 700)
    #win.set_geometry_hints(300, 700)
    win.connect("delete-event", win.destroy)
    #win.gtk_window_set_decorated(False)
    win.show_all()
    win.resize(300, 700)
    win.reshow_with_initial_size()
    Gtk.Window.set_resizable(win, True)
    Gtk.main()

if __name__ == "__main__":
    main()
