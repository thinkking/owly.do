# -*- coding: utf-8 -*-
from gi.repository import Gtk, GObject, Gio
import os.path
import sys

class UI(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="OWL: Cal")
        self.set_border_width(3)

        #windows size
        self.set_default_size(700, 700)
        self.UIbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=3)
        self.add(self.UIbox)

        hb = Gtk.HeaderBar()
        hb.set_show_close_button(False)
        hb.props.title = "OWL: Cal"
        self.set_titlebar(hb)

        scrolled = Gtk.ScrolledWindow()
        scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

#headerbar packing
        control_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(control_box.get_style_context(), "linked")
            #right btutton set //autopilot & gears
        newevent_btn = Gtk.Button()
        newevent_icon = Gio.ThemedIcon(name="application-add-symbolic")
        newevent_image = Gtk.Image.new_from_gicon(newevent_icon,
             Gtk.IconSize.BUTTON)
        newevent_btn.add(newevent_image)
        newevent_btn.set_tooltip_text("restart IPSHIELD")
        control_box.add(newevent_btn)

        hb.pack_end(control_box)

            #left button set //Calendar & Progress
        viewset_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(viewset_box.get_style_context(), "linked")

        back2main_btn = Gtk.Button()
        back2main_icon = Gio.ThemedIcon(name="go-previous-symbolic")
        back2main_image = Gtk.Image.new_from_gicon(back2main_icon,
             Gtk.IconSize.BUTTON)
        back2main_btn.add(back2main_image)
        back2main_btn.set_tooltip_text("view status")
        viewset_box.add(back2main_btn)
        back2main_btn.connect("clicked", self.destroy)

        clearsome_btn = Gtk.Button()
        clearsome_icon = Gio.ThemedIcon(name="edit-clear-all-symbolic")
        clearsome_image = Gtk.Image.new_from_gicon(clearsome_icon,
             Gtk.IconSize.BUTTON)
        clearsome_btn.add(clearsome_image)
        clearsome_btn.set_tooltip_text("view status")
        viewset_box.add(clearsome_btn)

        hb.pack_start(viewset_box)

        try:
            self.set_icon_from_file("OWL.png")
        except Exception as e:
            print (e.message)
            sys.exit(1)

    def destroy(self, event, param=None):
        Gtk.main_quit()


def Calvin():
    Calvin = UI()
    Calvin.connect("delete-event", Calvin.destroy)
    Calvin.show_all()